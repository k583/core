from django.contrib import admin

from .models import Client
from .models import Device , DevicesTable
from django.utils import timezone

#admin.site.register(Client)
#admin.site.register(Device)
#admin.site.register(DevicesTable)


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('service_name', 'city', 'aaa','street')

    def aaa(self, object):
        return object.city + 'sss'
    aaa.short_description =  'krakonoš'

# Register the Admin classes for Client using the decorator
@admin.register(DevicesTable)
class DevicesTable(admin.ModelAdmin):
    list_display = ('aaa','bbb','calib_date','next_date','next_date_aaa')
    @admin.display(ordering='calib_date')
    def aaa(self,obj):
        return obj.client.service_name
    def bbb(self,obj):
        return obj.client.city
    def next_date_aaa(self,obj):
        aaa='?'
        if obj.device.time_interval == '1Y':
            aaa = timezone.now() + timezone.timedelta(years=1)
        elif obj.device.time_interval == '2Y':
            aaa = timezone.now() + timezone.timedelta(years=2)
        elif obj.device.time_interval == '6M':
            aaa = timezone.now() + timezone.timedelta(mounths=6)
        return aaa

    # def CalulateNextCalibData(self):
    #     if obj.self.device.type == '1Y':
    #         aaa = timezone.now() + timezone.timedelta(years=1)
    #     elif obj.self.device.type == '2Y':
    #         aaa = timezone.now() + timezone.timedelta(years=2)
    #     elif obj.self.device.type == '6M':
    #         aaa = timezone.now() + timezone.timedelta(mounths=6)
    #     return aaa
    # def get_changeform_initial_data(self, request):
    #     print(dir(request))
    #     # return {'next_date':CalulateNextCalibData()}
    #     return {'next_date':timezone.now()}


#    def get_changeform_initial_data(self, request):
#    return {'name': 'custom_initial_value'}

#    list_display = ('Jméno zákazníka','Město','PSČ', 'Zařízení','sériové číslo')
#admin.site.register(Client, ClientAdmin)# Register the Admin classes for Device using the decorator
@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    pass
#admin.site.register(Device, DeviceAdmin)# Register the Admin classes for Device using the decorator

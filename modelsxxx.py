from django.db import models
from phone_field import PhoneField
from django.utils import timezone
import re
from django.core.exceptions import ValidationError


class Client(models.Model):
    def psc(val):
        if not re.match(r"^\d{3} \d{2}$", val):
            raise ValidationError('Tohlr psč není')

    service_name = models.CharField('service name',max_length=200)
    street = models.CharField('street', max_length=200)
    city = models.CharField('city',max_length=200)
    PSC = models.CharField('PSČ',max_length=6, default = '000 00', validators = [psc])
    phone_number = PhoneField(blank=True, help_text='Contact phone number')

    def __str__(self):
        return "%s %s" % (self.service_name, self.city)


class Device(models.Model):

    class TimeInterval(models.TextChoices):
        ONE_YEAR = '1Y', ('one year')
        TWO_YEARS = '2Y', ('two years')
        SIX_MONTHS = '6M', ('six mounths')

    time_interval = models.CharField(
        max_length=2,
        choices=TimeInterval.choices,
        default=TimeInterval.ONE_YEAR,
    )
    type = models.CharField(max_length=50)
    note = models.CharField(max_length=200)
    def __str__(self):
        return self.type


class DevicesTable(models.Model):
    def s_n(val):
        if not re.match(r"^\d{3}-\d{2}$", val):
            raise ValidationError('Tohle sériové číslo není')
    client = models.ForeignKey(Client, on_delete=models.PROTECT,)
    device = models.ForeignKey(Device, on_delete=models.PROTECT,)
    serial_number = models.CharField('sériové číslo',max_length=6, default = '000-00',validators = [s_n])
    calib_date = models.DateTimeField('date of calibration',default=timezone.now)
    next_date = models.DateTimeField('next date of calibration',blank=True, null=True)
    def __str__(self):
        return self.device.type


    def CalulateNextCalibData(self):
        if self.device.type == '1Y':
            self.next_date = timezone.now() + timezone.timedelta(years=1)
        elif self.device.type == '2Y':
            self.next_date = timezone.now() + timezone.timedelta(years=2)
        elif self.device.type == '6M':
            self.next_date = timezone.now() + timezone.timedelta(mounths=6)
        self.save()

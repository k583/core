from django.conf.urls import url
from . import views

urlpatterns = [
     # url(r'^$', views.calibration_list_table, name='calibration_list_table'),
     url(r'^$', views.device_list_table, name='device_list_table'),
     url(r'^calib/(?P<pk>[0-9]+)/$', views.calib_detail, name='calib_detail'),
     url(r'^device/(?P<pk>[0-9]+)/$', views.device_detail, name='device_detail'),
     url(r'^client/(?P<pk>[0-9]+)/$', views.client_detail, name='client_detail'),
     url('^calib/new/$', views.add_calibration, name='add_calibration'),
     url('^calib/now/(?P<pk>[0-9]+)$', views.now_calibrated, name='now_calibrated'),
     url('^email/$', views.send_email, name='send_email'),

]

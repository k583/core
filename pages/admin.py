from django.contrib import admin
from .models import Client, DeviceType, Device, Calibartion
# from django.utils import timezone
from simple_history.admin import SimpleHistoryAdmin


@admin.register(Client)
class ClientAdmin(SimpleHistoryAdmin):
    list_display = ('service_name', 'city', 'street')
    # list_display = ('service_name', 'city','street','aaa')

    # def aaa(self, object):
    #     return object.city + 'sss'
    # aaa.short_description =  'krakonoš'

# Register the Admin classes for Client using the decorator


@admin.register(DeviceType)
class DeviceTypeAdmin(SimpleHistoryAdmin):
    list_display = ('type', 'note')
#    @admin.display(ordering='type')


@admin.register(Device)
class DeviceAdmin(SimpleHistoryAdmin):
    # ordering = ('last_calib','client','device_type','serial_number',)
    # pokud tam neni carka tak to nefunguje
    # ordering = ('last_calib',)
    list_display = ['last_calib', 'client', 'device_type', 'serial_number', ]

    def last_calib(self, object):
        a = Calibartion.objects.filter(
            device=object).order_by('-next_date')[:1]
        if a is None or len(a) == 0:
            return None
        else:
            return a[0].next_date


@admin.register(Calibartion)
class CalibartionAdmin(SimpleHistoryAdmin):
    ordering = ('next_date',)  # pokud tam neni carka tak to nefunguje
    list_display = ['client', 'device__type', 'next_date', 'calib_date']

    def client(self, object):
        return object.device.client

    def device__type(self, object):
        return object.device

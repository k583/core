from django.shortcuts import render
from .models import Client,DeviceType,Device,Calibartion
from django.utils import timezone
from .forms import CalibartionForm, NowCalibartionForm,DeviceTypeForm,ClientForm, EmailForm
from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect
from django.forms import inlineformset_factory

from django.conf import settings
from django.core.mail import send_mail



# Create your views here.
def calibration_list_table(request):
   posts = Calibartion.objects.filter(calib_date__lte=timezone.now()) # data ktera maji nejaky datum kalibarce
   return render(request, 'pages/calibration_list_table.html', {'posts': posts})

# Create your views here.
def device_list_table(request):
    if request.method == "POST":
         if request.POST.get("send_email"):
             checkboxes_selected = request.POST.getlist('email')
             checkboxes_selected = list(dict.fromkeys(checkboxes_selected))
             checkboxes_selected = list(filter(None, checkboxes_selected)) # Remove any duplicates from a List:
             d = list(checkboxes_selected)
             print(d)
             print('--posli email ---')
             request.session['email'] = d
             return redirect('send_email')
             # https://stackoverflow.com/questions/8931040/django-redirect-with-kwarg
#pokracovat odchytit emaily, vyfiltrovat duplicitu, otevrit formular a dat tam tyto emaily.


    devices = Device.objects.all()
    for d in devices:
        a = Calibartion.objects.filter(device=d).order_by('-next_date')[:1]
        if a is None or len(a) ==0:
            d.last_calibration = None
        else:
            d.last_calibration = a[0]

    def aaa(x):
        if x.last_calibration is not None:
            return x.last_calibration.next_date
        else:
            return timezone.now()
    d = list(devices)
    print(d)
    d.sort(key=aaa, reverse=False)
    print(d)
    return render(request, 'pages/device_list_table.html', {'devices': d})

def add_calibration(request):
    if request.method == "POST":
        form = CalibartionForm(request.POST)
        if form.is_valid():
            post = form.save()
            post.calulate_next_calib_data()
            post.save()
            return redirect('calibration_list_table')
    else:
      form = CalibartionForm()
      return render(request, 'pages/add_calibration.html', {'form': form})

#Potom dodelat pk, abych mela pekne vyplnene zarizeni
def now_calibrated(request, pk):
    # post = get_object_or_404(Calibartion, pk=pk)
    calib = Calibartion.objects.get(pk=pk)
    if request.method == "POST":
        form = CalibartionForm(request.POST)
        # form.device = calib.device
        if form.is_valid():
            post = form.save()
            post.calulate_next_calib_data()
            # print(post.device)
            post.save()
            return redirect('calibration_list_table')
    else:
      initial = {
        'device': calib.device,
        'calib_date' : timezone.now()
      }
      form = CalibartionForm(request.POST or None, initial = initial)
      print(form)
      return render(request, 'pages/calib_now.html', {'form': form})

def calib_detail(request, pk):
     post = get_object_or_404(Calibartion, pk=pk)
     return render(request, 'pages/calib_detail.html', {'post': post})

def device_detail(request, pk):
     device_item = Device.objects.get(pk=pk)
     device_type_item = device_item.device_type
     form = DeviceTypeForm(instance=device_type_item)
     return render(request, 'pages/device_detail.html', {'form': form})

def client_detail(request, pk):
    device_item = Device.objects.get(pk=pk)
    device_client_item = device_item.client
    form = ClientForm(instance=device_client_item)
    return render(request, 'pages/client_detail.html', {'form': form})

def send_email(request):
      # create a variable to keep track of the form
    print('----fce send_email ------')
    print(request.session.get('email'))
    d = request.session.get('email')
    messageSent = False

    # check if form has been submitted
    if request.method == 'POST':


        form = EmailForm(request.POST)

        # check if data from the form is clean
        if form.is_valid():
            cd = form.cleaned_data
            subject = "Sending an email with Django"
            message = cd['message']

            # send the email to the recipent
            send_mail(subject, message,
                      settings.DEFAULT_FROM_EMAIL, [cd['recipient']])

            # set the variable initially created to True
            messageSent = True

    else:
        print('----fce send_email ------')
        # email_list = request.getlist('recipient')
        # print(email_list)
        initial = {
        'recipient': d
        }
        form = EmailForm( initial = initial)
        form

    return render(request, 'pages/send_email.html', {
        'form': form,
        'messageSent': messageSent,

    })
#     if request.method == "POST":
#         print('--posli email ---')
#         if request.POST.get("send_email"):
#             print('--- pozadavek na email -----')
#
#     #
#     # device_item = Device.objects.get(pk=pk)
#     # email = device_item.client.email
#     # subject = 'welcome to django pages'
#     # message = f'Hi, I am genius.'
#     # email_from = settings.EMAIL_HOST_USER
#     # recipient_list = ['ada11@centrum.cz', ]
#     # send_mail( subject, message, email_from, recipient_list )
#     return redirect('device_list_table')

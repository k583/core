from django import forms

from .models import Client, DeviceType, Device, Calibartion
# from multi_email_field.forms import MultiEmailField


class CalibartionForm(forms.ModelForm):
    class Meta:
        model = Calibartion
        fields = ('device', 'calib_date')


class NowCalibartionForm(forms.ModelForm):
    class Meta:
        model = Calibartion
        fields = ('calib_date',)


class DeviceTypeForm(forms.ModelForm):
    class Meta:
        model = DeviceType
        fields = ('time_interval', 'type', 'note')


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('service_name', 'street', 'city',
                  'phone_number', 'psc_number', 'email',)


class EmailForm(forms.Form):
    subject = forms.CharField(
        label='Subject', max_length=100, widget=forms.Textarea)
    recipient = forms.CharField(max_length=100, widget=forms.Textarea)
    message = forms.CharField(widget=forms.Textarea)

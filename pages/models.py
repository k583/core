from django.db import models
from phone_field import PhoneField  # pip install django-phone-field
# from django.utils import timezone
from dateutil.relativedelta import relativedelta  # $ pip install python-dateutil
import re
from django.core.exceptions import ValidationError
from simple_history.models import HistoricalRecords


class Client(models.Model):

    def psc(val):
        if not re.match(r"^\d{3} \d{2}$", val):
            raise ValidationError('Tohle psč není')
    service_name = models.CharField('Název servisu', max_length=200)
    street = models.CharField('ulice', max_length=200)
    city = models.CharField('město', max_length=200)
    phone_number = PhoneField(blank=True, help_text='Contact phone number')
    psc_number = models.CharField(
        'PSČ', max_length=6, default='000 00', validators=[psc])
    email = models.EmailField(blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return "%s %s" % (self.service_name, self.city)


class DeviceType(models.Model):

    class TimeInterval(models.TextChoices):
        ONE_YEAR = '1Y', ('one year')
        TWO_YEARS = '2Y', ('two years')
        SIX_MONTHS = '6M', ('six mounths')

    time_interval = models.CharField(
        max_length=2,
        choices=TimeInterval.choices,
        default=TimeInterval.ONE_YEAR,
    )
    type = models.CharField(max_length=50)
    note = models.CharField(max_length=200)
    history = HistoricalRecords()

    def __str__(self):
        return self.type


class Device(models.Model):
    def s_n(val):
        if not re.match(r"^\d{3}-\d{2}$", val):
            raise ValidationError('Tohle není sériové číslo')
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    device_type = models.ForeignKey(DeviceType, on_delete=models.PROTECT)
    serial_number = models.CharField(
        'sériové číslo', max_length=6, default='000-00', validators=[s_n])
    history = HistoricalRecords()
#    calib_date = models.DateTimeField('date of calibration',default=timezone.now)
#    next_date = models.DateTimeField('next date of calibration',blank=True, null=True)

    def __str__(self):
        return "%s %s" % (self.device_type.type, self.serial_number)


class Calibartion(models.Model):
    device = models.ForeignKey(Device, on_delete=models.PROTECT,)
    calib_date = models.DateTimeField('date of calibration')
    next_date = models.DateTimeField(
        'next date of calibration', blank=True, null=True)
    history = HistoricalRecords()

    def calulate_next_calib_data(self):
        now = self.calib_date
        if self.device.device_type.time_interval == '1Y':
            self.next_date = now.replace(year=now.year + 1)
        elif self.device.device_type.time_interval == '2Y':
            self.next_date = now.replace(year=now.year + 2)
        elif self.device.device_type.time_interval == '6M':
            self.next_date = now + relativedelta(months=6)
        self.save()
